package com.example.preexamen9_3c2;

import java.io.Serializable;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Usuario implements Serializable {
    public int idUsuario;

    public String usuario;
    public String email;
    public String contrasena;

    //public String idcelular;

    //public String perfil;

   // public Date fecharegistro;

    public Usuario() {
        this.usuario = "";
        this.contrasena = "";
        this.email = "";
       /* try {
            java.text.SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
            this.fecharegistro = formatoFecha.parse("01/01/1970");
        } catch (ParseException e) {
            e.printStackTrace();
        }*/

    }

    public Usuario(int idUsuario, String usuario, String email, String contrasena){
        this.idUsuario = idUsuario;
        this.usuario = usuario;
        this.email = email;
        this.contrasena= contrasena;
        //this.fecharegistro = fecharegistro;


    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public String getContrasena() {

        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getUsuario() {

        return usuario;
    }

    public void setUsuario(String usuario) {

        this.usuario = usuario;
    }
    /*public String getIdCelular() {
        return idcelular;
    }
    public void setIdCelular(String idcelular) {
        this.idcelular = idcelular;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }


    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }*/

}

