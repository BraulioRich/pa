package com.example.preexamen9_3c2.modelo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class UsuarioDbHelper extends SQLiteOpenHelper {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ", ";

    private static final String SQL_CREATE_USUARIO = "CREATE TABLE " +
            DefineTable.Usuarios.TABLE_NAME + " ( " +
            DefineTable.Usuarios.COLUMN_NAME_IDUSUARIO + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DefineTable.Usuarios.COLUMN_NAME_USUARIO + TEXT_TYPE + COMMA_SEP +
            DefineTable.Usuarios.COLUMN_NAME_EMAIL + TEXT_TYPE + COMMA_SEP +
            DefineTable.Usuarios.COLUMN_NAME_CONTRASENA + TEXT_TYPE + " );";
            //DefineTable.Usuarios.COLUMN_NAME_IDCELULAR + TEXT_TYPE +
            //DefineTable.Usuarios.COLUMN_NAME_PERFIL + TEXT_TYPE + COMMA_SEP +
            //DefineTable.Usuarios.COLUMN_NAME_FECHAREGISTRO + "DATE DEFAULT CURRENT_DATE" + COMMA_SEP + " );";

    private static final String SQL_DELETE_USUARIO = "DROP TABLE IF EXISTS " +
            DefineTable.Usuarios.TABLE_NAME;

    private static final String DATABASE_NAME = "usuarios55.db";
    private static final int DATABASE_VERSION = 1;

    public UsuarioDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_USUARIO);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_USUARIO);
        onCreate(db);
    }
}
