package com.example.preexamen9_3c2.modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.preexamen9_3c2.Usuario;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class UsuariosDb implements Persistencia, Proyeccion {

    private Context context;
    private UsuarioDbHelper helper;
    private SQLiteDatabase db;

    public UsuariosDb(Context context, UsuarioDbHelper helper){
        this.context = context;
        this.helper = helper;
    }

    public UsuariosDb(Context context){
        this.context = context;
        this.helper = new UsuarioDbHelper(this.context);
    }

    @Override
    public void openDataBase() {
        db = helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    @Override
    public long insertUsuario(Usuario usuario) {
        ContentValues values = new ContentValues();

        values.put(DefineTable.Usuarios.COLUMN_NAME_USUARIO, usuario.getUsuario());
        values.put(DefineTable.Usuarios.COLUMN_NAME_EMAIL, usuario.getEmail());
        values.put(DefineTable.Usuarios.COLUMN_NAME_CONTRASENA, usuario.getContrasena());
        //values.put(DefineTable.Usuarios.COLUMN_NAME_IDCELULAR, usuario.getIdCelular());
        //values.put(DefineTable.Usuarios.COLUMN_NAME_PERFIL, usuario.getPerfil());
        //values.put(DefineTable.Usuarios.COLUMN_NAME_FECHAREGISTRO, usuario.getFecharegistro().getTime());



        this.openDataBase();
        long num = db.insert(DefineTable.Usuarios.TABLE_NAME, null, values);
        return num;
    }

    @Override
    public long updateUsuario(Usuario usuario) {
        ContentValues values = new ContentValues();

        values.put(DefineTable.Usuarios.COLUMN_NAME_IDUSUARIO, usuario.getIdUsuario());
        values.put(DefineTable.Usuarios.COLUMN_NAME_USUARIO, usuario.getUsuario());
        values.put(DefineTable.Usuarios.COLUMN_NAME_EMAIL, usuario.getEmail());
        values.put(DefineTable.Usuarios.COLUMN_NAME_CONTRASENA, usuario.getContrasena());
        //values.put(DefineTable.Usuarios.COLUMN_NAME_IDCELULAR, usuario.getIdCelular());
        //values.put(DefineTable.Usuarios.COLUMN_NAME_PERFIL, usuario.getPerfil());
        //values.put(DefineTable.Usuarios.COLUMN_NAME_FECHAREGISTRO, usuario.getFecharegistro().getTime());


        this.openDataBase();
        long num = db.update(
                DefineTable.Usuarios.TABLE_NAME,
                values,
                DefineTable.Usuarios.COLUMN_NAME_IDUSUARIO + " = " + usuario.getIdUsuario(),
                null);
        return num;
    }

    @Override
    public void deleteUsuario(int getIdUsuario) {
        this.openDataBase();
        db.delete(
                DefineTable.Usuarios.TABLE_NAME,
                DefineTable.Usuarios.COLUMN_NAME_IDUSUARIO + "=?",
                new String[] {String.valueOf(getIdUsuario)}
        );
    }

    @Override
    public Usuario getUsuario(String email) {
        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTable.Usuarios.TABLE_NAME,
                DefineTable.Usuarios.REGISTRO,
                DefineTable.Usuarios.COLUMN_NAME_EMAIL + "=?",
                new String[] {email},
                null, null, null

        );

        if (cursor.moveToFirst()) {
            Usuario usuario = readUsuario(cursor);
            cursor.close();
            return usuario;
        } else {
            cursor.close();
            return null;
        }
    }

    @Override
    public List<Usuario> allUsuarios() {
        db = helper.getWritableDatabase();
        Cursor cursor = db.query(
                DefineTable.Usuarios.TABLE_NAME,
                DefineTable.Usuarios.REGISTRO,
                null, null, null, null, null
        );
        List<Usuario> usuarios = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                Usuario usuario = readUsuario(cursor);
                usuarios.add(usuario);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return usuarios;
    }

    @Override
    public Usuario readUsuario(Cursor cursor) {
        Usuario usuario = new Usuario();

        usuario.setIdUsuario(cursor.getInt(0));
        usuario.setUsuario(cursor.getString(1));
        usuario.setContrasena(cursor.getString(2));
        usuario.setEmail(cursor.getString(3));
        //usuario.setIdCelular(cursor.getString(4));
        //usuario.setPerfil(cursor.getString(5));
        /*long timestamp = cursor.getLong(6);
        Date fecharegistro = new Date(timestamp);
        usuario.setFecharegistro(fecharegistro);*/

        return usuario;
    }
}
