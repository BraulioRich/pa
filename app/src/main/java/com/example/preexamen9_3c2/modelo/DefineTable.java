package com.example.preexamen9_3c2.modelo;

import android.provider.BaseColumns;

public class DefineTable implements BaseColumns {
    public DefineTable(){}

    public static abstract class Usuarios {
        public static final String TABLE_NAME = "usuarios";
        public static final String COLUMN_NAME_IDUSUARIO = "idUsuario";
        public  static final String COLUMN_NAME_USUARIO = "usuario";
        public  static final String COLUMN_NAME_CONTRASENA = "contrasena";
        public static final String COLUMN_NAME_EMAIL = "email";

        //public static final String COLUMN_NAME_IDCELULAR = "idcelular";

       //public static final String COLUMN_NAME_PERFIL = "perfil";

        //public static final String COLUMN_NAME_FECHAREGISTRO = "fecharegistro";


        public static String[] REGISTRO = new String[]{
                Usuarios.COLUMN_NAME_IDUSUARIO,
                Usuarios.COLUMN_NAME_USUARIO,
                Usuarios.COLUMN_NAME_CONTRASENA,
                Usuarios.COLUMN_NAME_EMAIL,
                //Usuarios.COLUMN_NAME_IDCELULAR,
                //Usuarios.COLUMN_NAME_PERFIL,
                //Usuarios.COLUMN_NAME_FECHAREGISTRO

        };
    }
}
